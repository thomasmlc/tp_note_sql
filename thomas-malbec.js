const name = "thomas-malbec"
const promo = "B2B"

const q1 = `
SELECT *
FROM Track
WHERE Milliseconds < (
SELECT Milliseconds
FROM Track
WHERE TrackId = 3457
)
`
const q2 = `
SELECT *
FROM Track
WHERE MediaTypeId = (
	SELECT MediaTypeId FROM Track
	WHERE Name = 'Rehab')`
const q3 = `
SELECT  p.PlaylistId as "ID",p.Name as "nom", COUNT(pt.TrackId) as "Nombre de chansons",SUM(t.Milliseconds)/ 60000 as "Durée en minutes", AVG(t.Milliseconds)/ 60000 as "Durée moyenne"

FROM PlaylistTrack pt
JOIN Playlist p on p.PlaylistId = pt.PlaylistId
JOIN Track t on t.TrackId = pt.TrackId

GROUP BY p.Name, p.PlaylistId
`
const q4 = ``
const q5 = `SELECT PlaylistId
FROM PlaylistTrack
GROUP BY PlaylistId

HAVING COUNT(TrackId) =
    (SELECT COUNT(TrackId)
     FROM PlaylistTrack
     WHERE  PlaylistId IN (1, 13)
	 )`
const q6 = `
SELECT  c.* FROM Customer c
JOIN Invoice i ON i.CustomerId = c.CustomerId
WHERE i.Total > (
	SELECT MAX(Total) FROM Invoice
	WHERE BillingCountry = 'France')`
const q7 = `SELECT Distinct(BillingCountry), MIN(Total) as 'minimum', MAX(Total) AS 'MAX', AVG(Total) AS 'Moyenne de comande',COUNT(Total) AS 'TOTAL COMMANDE'
,SUM(Total)/(SELECT SUM(Total) FROM Invoice)*100 AS 'Pourcentage Total' 
,CAST(COUNT(Total)AS float)/(SELECT CAST(COUNT(Total)AS float)FROM Invoice)*100 AS 'Pourcentage nombre'  FROM Invoice
GROUP BY Invoice.BillingCountry;`
const q8 = `SELECT t.* ,mt.Name ,(SELECT AVG(t.UnitPrice)FROM Track t) FROM Track t
JOIN MediaType mt ON mt.MediaTypeId = t.MediaTypeId
WHERE t.UnitPrice > (SELECT AVG(t.UnitPrice) FROM Track t)

GROUP BY t.Name ,t.TrackId, t.AlbumId, t.MediaTypeId, t.GenreId, t.Composer, t.Milliseconds, t.Bytes, t.UnitPrice, mt.Name
`
const q9 = ``
const q10 = `SELECT DISTINCT p.Name,ar.Name AS 'artiste',COUNT(ar.Name) AS 'nombre de fois present dans la playlist' ,COUNT(a.Title)AS 'titre de cet artiste',AVG(t.UnitPrice) AS 'prix moyen titre'    FROM PlaylistTrack pt
JOIN Track t ON t.TrackId = pt.TrackId
JOIN Album a ON a.AlbumId = t.AlbumId
JOIN Artist ar ON ar.ArtistId = a.ArtistId
JOIN Playlist p ON pt.PlaylistId = p.PlaylistId
GROUP BY p.Name,ar.Name`
const q11 = ``
const q12 = ``
const q13 = ``
const q14 = `SELECT AVG(t.UnitPrice)AS 'moyenne du prix', SUM(t.Milliseconds)/60000 AS 'durée total en minutes', SUM(t.UnitPrice)/(SELECT SUM(t.Milliseconds)/60000) AS 'prix par secondes'   FROM Invoice i
JOIN InvoiceLine il ON il.InvoiceId = i.InvoiceId
JOIN Track t ON t.TrackId = il.TrackId
GROUP BY il.InvoiceId`
const q15 = ``
const q16 = ``
const q17 = ``
const q18 = ``
const q19 = `INSERT INTO Track (Name, Composer,UnitPrice,MediaTypeId,Milliseconds) VALUES ('Solvable', 'Niro, SCH',0.99,1,180000)
INSERT INTO Track (Name, Composer,UnitPrice,MediaTypeId,Milliseconds) VALUES ('Bon deja', 'Niska',0.99,1,180000)
INSERT INTO Track (Name, Composer,UnitPrice,MediaTypeId,Milliseconds) VALUES ('mechant', 'kodes',0.99,1,180000)`
const q20 = ``
const q21 = `DELETE FROM Invoice
WHERE YEAR(InvoiceDate) = '2010'`
const q22 = ``
const q23 = ``
const q24 = `ALTER TABLE Employee ADD Salary INT`
const q25 = ``
const q26 = `ALTER TABLE Invoice DROP COLUMN BillingPostalCode `











































// NE PAS TOUCHER CETTE SECTION
const tp = {name: name, promo: promo, queries: [q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20, q21, q22, q23, q24, q25, q26]}
module.exports = tp
